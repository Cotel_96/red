package pract7;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

public class Ejercicio2 {

	public static void main(String[] args) {
		ServerSocket ss;
		try {
			ss = new ServerSocket(7777);
			ServidoruEcho uEcho = new ServidoruEcho();
			uEcho.start();
			while(true){
				Socket cli = ss.accept();
				ServidortEcho c = new ServidortEcho(cli);
				c.start();		
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
			
	}

}
