package pract7;

import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.SocketException;

public class ServidoruEcho extends Thread{
	public void run() {
		try {
			DatagramSocket ss = new DatagramSocket(8888);
			byte[] buf = new byte[1000];
			DatagramPacket res = new DatagramPacket(buf, 1000);
			while(true) {				
				ss.receive(res);
				ss.send(res);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
