package pract7;

import java.io.IOException;
import java.io.PrintWriter;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.Scanner;

public class Ejercicio1 extends Thread {
	Socket cliente;
	Scanner in;
	
	public Ejercicio1(Socket c) throws IOException {
		this.cliente = c;
		this.in = new Scanner(c.getInputStream());
	}
	
	public void run() {
		while(in.hasNext()) {
			System.out.println(in.nextLine());
		}
	}

	public static void main(String[] args) {
		Socket cliente;
		Scanner teclado;
		PrintWriter out;
		try {
			cliente = new Socket("localhost", 7777);
			out = new PrintWriter(cliente.getOutputStream(), true);
			teclado = new Scanner(System.in);
			
			Ejercicio1 cl = new Ejercicio1(cliente);
			cl.start();
			
			while(teclado.hasNext()) {
				String msg = teclado.nextLine();
				out.println(msg);
				if(msg.equals("quit")) {
					break;
				}
			}
			
			cliente.close();
		} catch (UnknownHostException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
	}

}
