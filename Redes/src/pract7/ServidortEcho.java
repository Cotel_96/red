package pract7;

import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Scanner;

public class ServidortEcho extends Thread {
	private Socket cliente;	

	public ServidortEcho(Socket cliente) {
		this.cliente = cliente;	
	}		
	
	public void run() {
		try {
			PrintWriter pw = new PrintWriter(cliente.getOutputStream(), true);
			Scanner in = new Scanner(cliente.getInputStream());
			while(in.hasNext()) {
				pw.println(in.nextLine());
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
