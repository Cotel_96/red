package pract5;

import java.io.IOException;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Scanner;

public class Ejercicio3 extends Thread{
	Socket s;
	int id;
	
	public Ejercicio3(Socket s, int id){
		this.s = s;
		this.id = id;
	}
	
	public void run() {
		String msg;
		Scanner sc;
		PrintWriter pw;
		try {
			do{
				sc = new Scanner(s.getInputStream());
				pw = new PrintWriter(s.getOutputStream(), true);
				msg = sc.nextLine();
				System.out.println(id+": "+msg);
				pw.println(msg);
			} while(!msg.equals("QUIT"));
			pw.println("Bye!");
			s.close();
			sc.close();
			pw.close();
			
						
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public static void main(String[] args) {
		try {
			int i = -1;
			ServerSocket ss = new ServerSocket(7777);
			while(true) {
				Socket s = ss.accept();
				i++;
				Ejercicio3 t = new Ejercicio3(s, i);
				t.start();
			}
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

}
