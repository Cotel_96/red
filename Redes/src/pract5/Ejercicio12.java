package pract5;

public class Ejercicio12 extends Thread{
	private String msg;
	
	public Ejercicio12(String m) {
		msg = m;
	}
	
	public void run() {		
		try {
			sleep(100);
			System.out.println(this.msg);			
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

	public static void main(String[] args) {
		for(int i=0; i<10; i++) {
			Ejercicio12 e = new Ejercicio12("Hola");
			e.start();
			System.out.println("Adi�s");
		}

	}

}
