package ejemplos;

import java.io.DataInputStream;
import java.io.IOException;
import java.io.PrintStream;
import java.net.Socket;

public class clientThread extends Thread{
	private DataInputStream is = null;
	private PrintStream os;
	private Socket clientSocket;
	private final clientThread[] threads;
	private int maxClientsCount;
	private String clientName;
	
	public clientThread(Socket client, clientThread[] threads) {
		this.clientSocket = client;
		this.threads = threads;
		maxClientsCount = threads.length;
	}
	
	/** Checkea si el string tiene caracteres especiales **/
	public boolean checkCorrect(String s) {
		if(s.equals("") || s.equals("\n")){
			return false;
		}
		
		for(int i=0; i<s.length(); i++) {
			if(!Character.isLetterOrDigit(s.charAt(i))) {
				return false;
			}
		}
		return true;
	}
	
	/** Checkea si el mensaje es un comando **/
	public boolean checkCommand(String s) {
		if(s.equals("/list") || s.equals("") || s.equals("\n") || s.equals("/exit")){
			return true;
		} else return false;
	}
	
	@SuppressWarnings("deprecation")
	public void run() {
		int maxClientsCount = this.maxClientsCount;
		clientThread[] threads = this.threads;
		
		/** Inicio conexion **/
		try {
			is = new DataInputStream(clientSocket.getInputStream());
			os = new PrintStream(clientSocket.getOutputStream());
			String name;
			do {
				os.println("*** Introduce tu nombre ***");
				name = is.readLine().trim();
				if(checkCorrect(name)) {
					break;
				} else {
					os.println("*** El nombre no puede contener caracteres especiales ***");
					name = null;
				}
			} while(true);
			
			/** Bienvenida **/
			os.println("*** Hola " + name + " ***.\n*** Para salir escribe /quit en una nueva linea ***");
			os.println("*** Para ver los usuarios conectdos escribe /list ***");
			synchronized (this) {
				for (int i = 0; i < maxClientsCount; i++) {
					if(threads[i] != null && threads[i] == this) {
						clientName = "@" + name;
						break;
					}
				}
				for(int i=0; i<maxClientsCount; i++){
					if (threads[i] != null && threads[i] != this) {
						threads[i].os.println("*** Un nuevo usuario ha entrado: " + name+ " ***");
					}
				}
			}
			
			/** Comprobación de un mensaje **/
			while(true) {
				String line = is.readLine();
				if(line.startsWith("/quit")) {				// Salida
					break;
				}
				if(line.startsWith("/list")){				// Consula usuarios
					for(int i=0; i<maxClientsCount; i++) {
						if(threads[i] != null && threads[i] != this)
							os.println(threads[i].clientName);
					}
				}
				if(line.equals("") || line.equals("\n")) {  // Checkea si el mensaje esta vacio
					os.println("*** No se pueden mandar mensajes vacios ***");
				}
				if(line.startsWith("@")) {					// Mensaje privado
					String[] words = line.split("\\s", 2);
					if(words.length > 1 && words[1] != null) {
						words[1] = words[1].trim();
						if(!words[1].isEmpty()) {
							synchronized (this) {
								for(int i=0; i<maxClientsCount; i++) {
									if(threads[i] != null && threads[i] != this
											&& threads[i].clientName != null
											&& threads[i].clientName.equals(words[0])){
										threads[i].os.println("<"+name+"> "+words[1]);
										this.os.println(">"+name+"> "+words[1]);
										break;
									}
								}
							}
						}
					}
				} else {									// Mensaje valido
					synchronized (this) {
						if(!checkCommand(line)) {
							for(int i=0; i<maxClientsCount; i++) {
								if(threads[i] != null && threads[i].clientName != null) {
									threads[i].os.println("<"+name+"> "+line);
								}
							}
						}
					}				
				}				
			}
			
			/** Salida de usuario **/
			synchronized (this) {
				for (int i = 0; i < maxClientsCount; i++) {
					if (threads[i] != null && threads[i] != this) {
						threads[i].os.println("*** Usuario " + name + " ha salido ***");

					}
				}
			}
			os.println("*** Adios " + name+ " ****");
			
			synchronized (this) {
				for (int i = 0; i < maxClientsCount; i++) {
					if (threads[i] == this) {
						threads[i] = null;
					}
				}
			}
			is.close();
			os.close();
			clientSocket.close();
		} catch (IOException e) {
			
		}
	}
}
