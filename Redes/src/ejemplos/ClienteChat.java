package ejemplos;

import java.awt.Button;
import java.awt.Frame;
import java.awt.TextArea;
import java.awt.TextField;
import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.net.Socket;
import java.net.UnknownHostException;

public class ClienteChat implements Runnable{
	
	private static Socket clientSocket = null;
	private static DataInputStream is = null;
	private static PrintStream os = null;
	private static BufferedReader inputLine = null;
	private static boolean closed = false;
	
	
	public static void main(String[] args) {
		int portNumber = 7777;
		String host = "localhost";
		
		try {
			clientSocket = new Socket(host, portNumber);
			os = new PrintStream(clientSocket.getOutputStream());
			is = new DataInputStream(clientSocket.getInputStream());
			inputLine = new BufferedReader(new InputStreamReader(System.in));
		} catch (UnknownHostException e) {
			System.err.println("No se ha podido conectar al servidor");
		} catch (IOException e) {
			System.err.println("Ocurrio un error con el I/O");
		}
		
		if(clientSocket != null && os != null & is != null) {
			try {
				new Thread(new ClienteChat()).start();
				while(!closed) {
					os.println(inputLine.readLine().trim());
				}
				os.close();
				is.close();
				clientSocket.close();			
			} catch (IOException e) {
				System.err.println("IOException: "+e);
			}
		}
	}
	
	public void run() {
		String responseLine;
		try {
			while((responseLine = is.readLine()) != null) {
				System.out.println(responseLine);
				if(responseLine.indexOf("*** Adios") != -1)
					break;
			}
			closed = true;
		} catch (IOException e) {
			System.err.println("IOException: " +e);
		}
	}
}