package ejemplos;

import java.io.IOException;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;

public class Chat {
	private static ServerSocket serverSocket = null;
	private static Socket clientSocket = null;
	
	private static final int maxClientsCount = 20;
	private static final clientThread[] threads = new clientThread[maxClientsCount];
	
	public static void main(String[] args) {
		int portNumber = 7777;
		try {
			serverSocket = new ServerSocket(portNumber);
		} catch (IOException e) {
			System.out.println(e);
		}
		
		while(true) {
			try {
				clientSocket = serverSocket.accept();
				int i=0;
				for(i = 0; i<maxClientsCount;i++) {
					if(threads[i]==null) {
						(threads[i] = new clientThread(clientSocket, threads)).start();
						break;
					}
				}
				
				if(i == maxClientsCount) {
					PrintWriter os = new PrintWriter(clientSocket.getOutputStream());
					os.println("Servidor muy ocupado");
					os.close();
					clientSocket.close();
				}
			} catch (IOException e) {
				System.out.println(e);
			}
		}
	}

}
