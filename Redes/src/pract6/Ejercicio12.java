package pract6;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.Arrays;
import java.util.Scanner;

public class Ejercicio12 {
	
	public static void ejercicio2() throws UnknownHostException {
		String google = "www.google.es";
		InetAddress[] direcciones = InetAddress.getAllByName(google);
		System.out.println("Las direcciones para " + google + " son: " + Arrays.toString(direcciones));
	}

	public static void main(String[] args) {
		
		Scanner input = new Scanner(System.in);
		System.out.print("Introduce una url: ");
		String url = input.nextLine();
		try {
			InetAddress res = InetAddress.getByName(url);
			System.out.println("La ip para ese dominio es: " + res.getHostAddress());
			ejercicio2();
		} catch (UnknownHostException e) {
			System.err.println("No se encontro ningun host con esa url");
		}
		
	}
}
