package pract6;

import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;

public class Ejercicio5 {
	
	public static void main(String[] args) {
		try {
			DatagramSocket cliente = new DatagramSocket();
			String txt = "Hola";
			cliente.send(new DatagramPacket(txt.getBytes(), txt.getBytes().length, InetAddress.getByName("localhost"), 7777));
			byte[] buffer = new byte[1000];
			DatagramPacket rec = new DatagramPacket(buffer, 1000);
			cliente.receive(rec);
			System.out.println(new String(rec.getData(),0,rec.getLength()));
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
		
}
