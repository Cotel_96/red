package pract6;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;
import java.net.UnknownHostException;

public class Ejercicio34 {
	
	public static DatagramPacket Ejercicio4() {
		try {
			String nombre = "Miguel Coleto";
			DatagramPacket paquete = new DatagramPacket(nombre.getBytes(), nombre.getBytes().length,
														InetAddress.getByName("localhost"), 7777);
			return paquete;
		} catch (UnknownHostException e) {
			e.printStackTrace();
		}
		return null;
		
	}
	
	public static void main(String[] args) throws SocketException {
		
		DatagramSocket udp = new DatagramSocket();
		System.out.println("Puerto del socket " + udp.getLocalPort());
		try {
			System.out.println("Mandando...");
			udp.send(new DatagramPacket("Hola".getBytes(), "Hola".getBytes().length,
										InetAddress.getByName("localhost"), 7777));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
	}
}
