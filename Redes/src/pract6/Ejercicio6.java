package pract6;

import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.util.Date;

public class Ejercicio6 {

	public static void main(String[] args) {
		try {
			DatagramSocket servidor = new DatagramSocket(7777);
			byte[] buffer = new byte[1000];
			DatagramPacket rec = new DatagramPacket(buffer, 1000);
			
			while(true){
				servidor.receive(rec);
				String fecha = (new Date().toString()) + "\n";
				servidor.send(new DatagramPacket(fecha.getBytes(),fecha.getBytes().length,rec.getAddress(),rec.getPort()));
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
}
